package com.example.hbh.finaltestapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText name;
    private TextView helpTv;
    private boolean isHelpTv = false;

    private static final String Test_Package = "com.example.testcase";
    private static final String Test_Class = "XQClickTest";

    private RunTestUtils.CMD_Result res;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            String resTitle = msg.getData().getString("CMD_RES_Title");
            String resInfo = msg.getData().getString("CMD_RES_Info");
            RunTestUtils.alertDialog(MainActivity.this,resTitle, resInfo);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

    }

    private void initView() {
        name = findViewById(R.id.packageName);
        helpTv = findViewById(R.id.help_text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_crawl:
                startCrawl();
                break;
            case R.id.help_btn:
                isHelpTv = !isHelpTv;
                if (isHelpTv){
                    helpTv.setVisibility(View.VISIBLE);
                }else {
                    helpTv.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void startCrawl() {
        new CrawlerThread().start();
    }

    private class CrawlerThread extends Thread{
        @Override
        public void run() {
            String packageName = name.getText().toString();
            String cmd = RunTestUtils.generateCommand(Test_Package, Test_Class, packageName);
            res = RunTestUtils.runCmd(cmd);
            if (res != null && res.resultCode == -1) {
                String resTitle = "";
                String resInfo = "";
                Message msg = new Message();
                Bundle b = new Bundle();
                resTitle = "测试失败[未找到包名]";
                resInfo = res.resInfo;

//                if (res.resultCode == 0) {
//                    resTitle = "测试完成";
//                    resInfo = "自动测试App[" + packageName + "]成功。";
//                } else
//                if (res.resultCode == -1){
//                    resTitle = "测试失败[未找到包名]";
//                    resInfo = res.resInfo;
//                }
                b.putString("CMD_RES_Title", resTitle);
                b.putString("CMD_RES_Info", resInfo);

                msg.setData(b);
                msg.what = 0;
                handler.sendMessage(msg);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String resInfo = getIntent().getStringExtra("result_info");
        if (!TextUtils.isEmpty(resInfo)){
            String resTitle = "测试完成";
            Message msg = new Message();
            Bundle b = new Bundle();
            msg.what = 1;
            b.putString("CMD_RES_Title", resTitle);
            b.putString("CMD_RES_Info", resInfo);
            msg.setData(b);
            handler.sendMessage(msg);
        }
    }
}
