package com.example.hbh.finaltestapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunTestUtils {

    private static final String Target_App_Pkg = "TargetAppPkg";
    private static final String PackageNameError = "Attempt to invoke virtual method 'int android.content.Intent.getFlags()' on a null object reference";

    public static String generateCommand(String tpkg, String tclass, String appPackage) {
        String command = "am instrument -w -r -e debug false -e class "
                + tpkg + "." + tclass + " " + "-e " + Target_App_Pkg +" " + appPackage + " "
                + tpkg + ".test/android.support.test.runner.AndroidJUnitRunner";
        return command;
    }

    public static CMD_Result runCmd(String cmd){
        CMD_Result result = null;
        Runtime runtime = Runtime.getRuntime();
        BufferedReader reader = null;
        int resultCode;
        try {
            Process process = runtime.exec(cmd);
            process.waitFor();
            StringBuilder resMsg = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            while ((s = reader.readLine()) != null) {
                resMsg.append(s);
            }
            Log.i("---------->", "runCmd:  " + resMsg.toString());
            // todo：各种错误的判断逻辑
            if (resMsg.toString().contains(PackageNameError)){
                resultCode = -1;
            }else {
                resultCode = 0;
            }
            result = new CMD_Result(resultCode,resMsg.toString() );

            reader.close();
        } catch (IOException | InterruptedException e) {
            result = new CMD_Result(-1, e.toString());
        }
        return result;
    }

    public static void alertDialog(final Context context, String title, String res){
        final AlertDialog.Builder builder =  new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(res);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.show();
    }

    public static class CMD_Result {
        public int resultCode;
        public String resInfo;

        public CMD_Result(int resultCode, String resInfo) {
            this.resultCode = resultCode;
            this.resInfo = resInfo;
        }
    }
}
