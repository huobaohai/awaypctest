package com.example.testcase;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class XQClickTest {

    private UiDevice mDevice;
    @Test
    public void useAppContext() throws UiObjectNotFoundException {


        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        String pkg = InstrumentationRegistry.getArguments().getString("TargetAppPkg");
        openApp(pkg);

        mDevice.waitForWindowUpdate(pkg, 5 * 2000);

        UiObject zixuan = mDevice.findObject(new UiSelector().text("自选"));
        zixuan.click();
        UiObject dongtai = mDevice.findObject(new UiSelector().text("动态"));
        dongtai.click();
        UiObject hangqing = mDevice.findObject(new UiSelector().text("行情"));
        hangqing.click();
        UiObject jiaoyi = mDevice.findObject(new UiSelector().text("交易"));
        jiaoyi.click();

        goBack(pkg);

    }

//    todo: 传值回去
    private void goBack(String pkg) {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.example.hbh.finaltestapp");
        intent.putExtra("result_info", "自动测试App[" + pkg + "]成功。");
        context.startActivity(intent);
    }

    private void openApp(String packageName) {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        context.startActivity(intent);
    }

}
